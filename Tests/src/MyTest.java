import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyTest
{
    @Test
    public void oneAndOneIsTwo()
    {
        assertEquals(3, Adder.add(1, 1));
    }
}